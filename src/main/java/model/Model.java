package model;


public class Model {

    private String name;
    private Double temp;
    private Double humidity;
    private String icon;
    private String main;
    private String country;

    public String getCountry(){
        return country;
    }

    public Double getHumidity() {
        return humidity;
    }

    public Double getTemp() {
        return temp;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public String getMain() {
        return main;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public void setCountry(String country){
        this.country = country;
    }
}

