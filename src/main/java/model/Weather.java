package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class Weather {
    //token 2f0d08c76efaad50690e41979dabe137
    //request http://api.openweathermap.org/data/2.5/weather?q=London&units=metric&appid=2f0d08c76efaad50690e41979dabe137
    public static String getWeather(String message, Model model) throws IOException {
        URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + message + "&units=metric&appid=2f0d08c76efaad50690e41979dabe137");

        Scanner in = new Scanner((InputStream) url.getContent());
        String result = "";
        while(in.hasNext()) {
            result += in.nextLine();
        }

        JSONObject object = new JSONObject(result);
        model.setName(object.getString("name"));

        JSONObject main = object.getJSONObject("main");
        model.setTemp(main.getDouble("temp"));
        model.setHumidity(main.getDouble("humidity"));

        JSONArray getArray = object.getJSONArray("weather");
        for(int i = 0; i <getArray.length(); i++){
            JSONObject obj = getArray.getJSONObject(i);
            model.setIcon((String) obj.get("icon"));
            model.setMain((String) obj.get("main"));
        }

        JSONObject getCountry = object.getJSONObject("sys");
        model.setCountry((String) getCountry.get("country"));
        return "City: " + model.getName() + "\n"  +
                "Country: " + model.getCountry() + "\n" +
                "Temperature: " + model.getTemp() +"C" + "\n" +
                "Humidity: " + model.getHumidity() + "%" + "\n" +
                "http://openweathermap.org/img/wn/" + model.getIcon() + ".png";
    }
}
